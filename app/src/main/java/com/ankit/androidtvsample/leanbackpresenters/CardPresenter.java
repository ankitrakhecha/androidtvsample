/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.ankit.androidtvsample.leanbackpresenters;

import android.graphics.drawable.Drawable;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.Presenter;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.ankit.androidtvsample.R;
import com.ankit.androidtvsample.models.ApiModel;
import com.ankit.androidtvsample.views.CustomImageCardView;

public class CardPresenter extends Presenter {
    private static final int CARD_WIDTH = 313;
    private static final int CARD_HEIGHT = 200;
    private static int sSelectedBackgroundColor;
    private static int sDefaultBackgroundColor;
    private static void updateCardBackgroundColor(CustomImageCardView view, boolean selected) {
        int color = selected ? sSelectedBackgroundColor : sDefaultBackgroundColor;
        view.setBackgroundColor(color);
        view.getTitleTextView().setBackgroundColor(color);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {

        sDefaultBackgroundColor =
                ContextCompat.getColor(parent.getContext(), R.color.default_background);
        sSelectedBackgroundColor =
                ContextCompat.getColor(parent.getContext(), R.color.selected_background);

        CustomImageCardView cardView = new CustomImageCardView(parent.getContext()){
            @Override
            public void setSelected(boolean selected) {
                updateCardBackgroundColor(this, selected);
                super.setSelected(selected);
            }
        };
        cardView.setFocusable(true);
        cardView.setFocusableInTouchMode(true);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ApiModel.CategoriesBean.VideosBean movie = (ApiModel.CategoriesBean.VideosBean) item;
        CustomImageCardView cardView = (CustomImageCardView) viewHolder.view;

        if (movie.getThumb() != null) {
            cardView.setCardModel(movie);
            cardView.setMainImageDimensions(CARD_WIDTH, CARD_HEIGHT);
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        CustomImageCardView cardView = (CustomImageCardView) viewHolder.view;
        cardView.setCardModel(null);
    }
}
