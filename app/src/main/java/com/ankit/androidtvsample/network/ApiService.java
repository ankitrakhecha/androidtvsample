package com.ankit.androidtvsample.network;

import com.ankit.androidtvsample.models.ApiModel;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ApiService {
    @GET("androidTvAssigment")
    Single<ApiModel> getData();
}
