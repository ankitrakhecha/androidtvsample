package com.ankit.androidtvsample.network;


import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;

import javax.inject.Inject;

public class PlayerRepository {

    private SimpleExoPlayer exoPlayer;
    @Inject
    public PlayerRepository(SimpleExoPlayer exoPlayer){
        this.exoPlayer = exoPlayer;
    }

    public SimpleExoPlayer getExoPlayer(){
        return exoPlayer;
    }
}
