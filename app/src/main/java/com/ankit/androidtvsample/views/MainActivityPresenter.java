package com.ankit.androidtvsample.views;

import android.util.Log;

import com.ankit.androidtvsample.AndroidTvSampleApplication;
import com.ankit.androidtvsample.models.ApiModel;
import com.ankit.androidtvsample.network.Repository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivityPresenter implements MainActivityContract.Presenter {
    private MainActivityContract.View view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    Repository repository;

    public MainActivityPresenter(MainActivityContract.View view) {
        this.view = view;
        AndroidTvSampleApplication.getDagger2App().getDataComponent().inject(this);
    }

    @Override
    public void onShowData() {
        compositeDisposable.add(
                repository.getNetworkItems().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ApiModel>() {
                            @Override
                            public void onSuccess(ApiModel apiModel) {
                                view.showData(apiModel);
                            }
                            @Override
                            public void onError(Throwable e) {
                                Log.e("TAG", "onError: "+e.getMessage() );
                            }
                        })
        );
    }

    @Override
    public void clear() {
        compositeDisposable.dispose();
    }
}
