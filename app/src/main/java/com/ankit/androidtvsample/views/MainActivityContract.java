package com.ankit.androidtvsample.views;


import com.ankit.androidtvsample.models.ApiModel;

public interface MainActivityContract {
     interface Presenter {
        void onShowData();
        void clear();
    }
     interface View {
        void showData(ApiModel apiModel);
    }

}
