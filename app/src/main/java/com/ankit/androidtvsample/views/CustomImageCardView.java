package com.ankit.androidtvsample.views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v17.leanback.widget.BaseCardView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ankit.androidtvsample.R;
import com.ankit.androidtvsample.databinding.CustomImageCardViewBinding;
import com.ankit.androidtvsample.models.ApiModel;

public class CustomImageCardView extends BaseCardView {
    private CustomImageCardViewBinding customImageCardViewBinding;

    public CustomImageCardView(Context context)
    {
       super(context);
        buildImageCardView(context);
    }
    private void buildImageCardView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        customImageCardViewBinding = DataBindingUtil.inflate(inflater,R.layout.custom_image_card_view,this,true);
    }
    public void setMainImageDimensions(int width, int height) {
        ViewGroup.LayoutParams lp = customImageCardViewBinding.customCardViewMainImage.getLayoutParams();
        lp.width = width;
        lp.height = height;
        customImageCardViewBinding.customCardViewMainImage.setLayoutParams(lp);
    }
    public void setCardModel(ApiModel.CategoriesBean.VideosBean videoBean){
        customImageCardViewBinding.setVideosBean(videoBean);
    }
    public TextView getTitleTextView(){
        return customImageCardViewBinding.customCardViewTitle;
    }
}
