
package com.ankit.androidtvsample.views;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.ankit.androidtvsample.R;


public class MainActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
