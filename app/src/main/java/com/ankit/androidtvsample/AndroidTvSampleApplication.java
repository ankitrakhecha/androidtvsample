package com.ankit.androidtvsample;

import android.app.Application;

import com.ankit.androidtvsample.dependecyinjection.DaggerDataComponent;
import com.ankit.androidtvsample.dependecyinjection.DataComponent;
import com.ankit.androidtvsample.dependecyinjection.DataModule;
import com.ankit.androidtvsample.dependecyinjection.PlayerModule;

public class AndroidTvSampleApplication extends Application {
    private static AndroidTvSampleApplication androidTvSampleApplication;
    DataComponent dataComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        androidTvSampleApplication = this;
        initDataComponent();
        dataComponent.inject(this);
    }
    public static AndroidTvSampleApplication getDagger2App(){
        return androidTvSampleApplication;
    }
    private void initDataComponent() {
        dataComponent = DaggerDataComponent.builder()
                .dataModule(new DataModule(this))
                .playerModule(new PlayerModule(this))
                .build();
    }
    public DataComponent getDataComponent() {
        return dataComponent;
    }
}
